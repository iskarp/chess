#include<string>
#include<iostream>
#include <vector>

class Piece{

public:
int x;
int y;
char type;
int colour;
bool is_attacking;

public:
Piece(int set_x, int set_y, char set_type, int set_colour, bool set_is_attacking) {
this->x = set_x;
this->y = set_y;
this->type = set_type;
this->colour = set_colour;
this->is_attacking = set_is_attacking;
}
void setX(int set_x) {
this-> x = set_x;
}
void setY(int set_y) {
this-> y = set_y;

}
char getType() {
return this->type;
}
int getX() {
return this-> x;
}
int getY() {
return this-> y;
}
void setAttacking(bool is_att) {
this->is_attacking =is_att;
}
bool getAttacking() {
return this-> is_attacking;
}
void setColour(int set_colour) {
this->colour = set_colour;
}
int getColour() {
return this->colour;
}
void setPosition(int new_x, int new_y) {
this->x = new_x;
this->y = new_y;
}
bool validateMove(int new_x, int new_y, char type) {
bool is_valid = false;
int curr_x = this->x;
int curr_y = this->y;
if((new_x<= 8 || new_x > 0) &&(new_y <= 8 || new_y > 0)) {
if(this->type == 'K') {

    if((curr_x + 1 == new_x) && (curr_y = new_y)) {
        is_valid = true;
    } else if(curr_x == new_x && curr_y + 1 == new_y) {
        is_valid = true;
    }  else if((curr_x -1 == new_x) && (curr_y = new_y)) {
        is_valid = true;
    }  else if(curr_x == new_x && curr_y -1 == new_y) {
        is_valid = true;
    }
} else if(this-> type = 'Q') {
    for(int i = 1; i <= 8; i++) {
        if(new_x == curr_x +i && new_y == curr_y+i) {
                is_valid = true;
        } else if (new_x == curr_x -i && new_y == curr_y-i) {
            is_valid = true;
        } else if (new_x == curr_x -i && new_y == curr_y+i) {
            is_valid = true;
        } else if (new_x == curr_x +i && new_y == curr_y-i) {
            is_valid = true;
        } else if (new_x == curr_x -i && new_y == curr_y) {
            is_valid = true;
        } else if (new_x == curr_x && new_y == curr_y+i) {
            is_valid = true;
        } else if (new_x == curr_x +i && new_y == curr_y) {
            is_valid = true;
        }else if (new_x == curr_x -i && new_y == curr_y) {
            is_valid = true;
        }else if (new_x == curr_x && new_y == curr_y-i) {
            is_valid = true;
        }
    } 
}else if (this-> type = 'B') {
    for(int i = 1; i <= 8; i++) {
        if(new_x == curr_x +i && new_y == curr_y+i) {
            is_valid = true;
        } else if (new_x == curr_x -i && new_y == curr_y-i) {
            is_valid = true;
        } else if (new_x == curr_x -i && new_y == curr_y+i) {
            is_valid = true;
        } else if (new_x == curr_x +i && new_y == curr_y-i) {
            is_valid = true;
        }
    }
} else if (this-> type = 'K') {
    if((curr_x +2 ==new_x) && (curr_y +1 ==new_y)) {
        is_valid = true;
    } else if((curr_x +2 ==new_x) && (curr_y -1 ==new_y)) {
        is_valid = true;
    } else if((curr_x -2 ==new_x) && (curr_y +1 ==new_y)) {
        is_valid = true;
    } else if((curr_x -2 ==new_x) && (curr_y -1 ==new_y)) {
        is_valid = true;
    } else if((curr_y +2 ==new_y) && (curr_x +1 ==new_x)) {
        is_valid = true;
    } else if((curr_y +2 ==new_y) && (curr_x -1 ==new_x)) {
        is_valid = true;
    } else if((curr_y -2 ==new_y) && (curr_x +1 ==new_x)) {
        is_valid = true;
    } else if((curr_y -2 ==new_y) && (curr_x -1 ==new_x)) {
        is_valid = true;
    }
} else if(this-> type = 'R') {
    for(int i=0; i <=8; i++) {
        if(new_x == curr_x + i && new_y == curr_y) {
            is_valid = true;
        } else if (new_x == curr_x - i && new_y ==curr_y) {
            is_valid = true;
        } else if (new_x == curr_x && new_y == curr_y +i) {
            is_valid = true;
        } else if (new_x ==  curr_x  && new_y == curr_y -i) {
            is_valid = true;
        }
    }
} else if (this-> type = 'P') {
    if(this->is_attacking == false) {
            for(int i = 1; i <= 8; i++) {
                if ((new_x ==  curr_x) && (new_y == curr_y +i)) {
                    is_valid = true;
                }
            }
    }

} else {
    if((new_x == curr_x +1) &&( new_y == curr_y +1)) {
        is_valid = true;
    }  else if((new_x == curr_x -1) && (new_y == curr_y +1)) {
        is_valid = true;
    }
}
}
return is_valid;
}

void moveX(int new_x, int new_y, char piece_type) {
    if(validateMove(new_x, new_y, piece_type)== true) {
    setPosition(new_x, new_y);
    } else {
    std::cout << "Invalid mvvement";
    }
}

};
class Board {
    public:
void printBoard() {
    for(int i = 8; i >= 1; i++) {
        for(int j = 'A'; j <= 'H'; j++) {
            if(this->board_pieces[(i-1)*(j-54)].getX() == j && this->board_pieces[(i-1)*(j-54)].getY() == i && j < 'H') {
                std::cout << b.this->board_pieces[(i-1)*(j-54)].getType();
            } else if(j%2 ==0 && i%2==0 && j != 7) {
                 std::cout << "#";
            } else if (j%2 !=0 && i%2!=0 && j != 7) {
                std::cout << " ";
            } else if(this->board_pieces[(i-1)*(j-54)].getX() == j && this->board_pieces[(i-1)*(j-54)].getY() == i && j == 'H') {
                std::cout << this->board_pieces[i*j].getType()<<std::endl;
            } else if(j%2 ==0 && i%2==0 && j == 7) {
                std::cout << "#"<<std::endl;
            } else if (j%2 !=0 && i%2!=0 && j != 7) {
                std::cout << " " <<std::endl;
            }
        
        }       
    }
}

int check_if_piece_in_a_way(int new_x, int new_y,int curr_x, int curr_y,
                    char piece_type , int piece_colour, std::vector<Piece> board_pieces) {

//0 empty 1 enemy 2 own 4 chess
    int something_in_a_way = 0;



    if(piece_type == 'K') {
        for(int i = 0; i < 32; i++) {
            if(new_x == this->this->board_pieces[i].getX() && (new_y = this->this->board_pieces[i].getY()) && this->this->board_pieces[i].getColour() != piece_colour) {
                std::cout <<"An enemy piece in a way.  At position: (" << this->this->board_pieces[i].getX() <<"," << this->this->board_pieces[i].getY() << ")";
                 something_in_a_way = 1;
                if(tthis->this->board_pieces[i].getType()=='K' && this->this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            } else if((new_x ==this-> this->board_pieces[i].getX()) && (new_y = this->this->board_pieces[i].getY()) && this->this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece on a way."<< "At position: (" << rthis->this->board_pieces[i].getX() <<"," << this->this->board_pieces[i].getY() << ")"<<std::endl;
                something_in_a_way = 2;
                break;
            }
        } 
    }else if(piece_type == 'Q') {
        for(int i = 1; i <= 8; i++) {
            if(new_x >curr_x && new_y > curr_y) {
            if(this->this->board_pieces[i].getX()== curr_x +i && this-> this->board_pieces[i].getY() == curr_y+i && this->this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->this->board_pieces[i].getX() <<"," << this->this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            } else if(this-> this->board_pieces[i].getX()== curr_x +i && this-> this->board_pieces[i].getY() == curr_y+i && this->this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in your target. At position: (" << this->this->board_pieces[i].getX() <<"," << this->this->board_pieces[i].getY() << ")";
                something_in_a_way = 1;
                if(this->this->board_pieces[i].getType()=='K' && this->this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            }
        } else if(new_x <curr_x && new_y > curr_y) {
            if(this->this->board_pieces[i].getX()== curr_x -i && this->this->board_pieces[i].getY() == curr_y+i && this->this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->this->board_pieces[i].getX() <<"," << this->this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                    break;
            }else if(this->this->board_pieces[i].getX()== curr_x -i && this->this->board_pieces[i].getY() == curr_y+i &&this-> this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in your targer. At position: (" <<this-> this->board_pieces[i].getX() <<"," <<this-> this->board_pieces[i].getY() << ")";
                    something_in_a_way = 1;
                        if(this->this->board_pieces[i].getType()=='K' && this->this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                        }
                    break;
            }
        } else if(new_x >curr_x && new_y < curr_y) {
                if(this->this->board_pieces[i].getX()== curr_x +i &&this-> this->board_pieces[i].getY() == curr_y-i && this->this->board_pieces[i].getColour() == piece_colour) {
                    std::cout << "Own piece in away At position: (" <<this-> this->board_pieces[i].getX() <<"," <<this-> this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                    break;
                }else if(this->this->board_pieces[i].getX()== curr_x +i && this->this->board_pieces[i].getY() == curr_y-i &&this-> this->board_pieces[i].getColour() != piece_colour) {
                    std::cout << "An enemy piece in a way. At position: (" <<this-> this->board_pieces[i].getX() <<"," <<this-> this->board_pieces[i].getY() << ")";
                    something_in_a_way = 1;
                    if(this->this->board_pieces[i].getType()=='K' && this->this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                        }
                    break;
                }
        }else if(new_x <curr_x && new_y == curr_y) {
            if(this->this->board_pieces[i].getX()== curr_x -i && this->this->board_pieces[i].getY() == curr_y && this->this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away. At position: (" << this->this->board_pieces[i].getX() <<"," <<this-> this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                    break;
            }else if(this->this->board_pieces[i].getX()== curr_x -i &&this-> this->board_pieces[i].getY() == curr_y && this->this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 1;
                    if(this->this->board_pieces[i].getType()=='K' && this->this->board_pieces[i].getColour() != piece_colour) {
                         something_in_a_way = 4;
                    }
                    break;
                std::cout << "An enemy piece in a way. At position: (" << this->this->board_pieces[i].getX() <<"," << this->this->board_pieces[i].getY() << ")";
            }  
        } else if(new_x =curr_x && new_y > curr_y) {
            if(this->this->board_pieces[i].getX()== curr_x && this->this->board_pieces[i].getY() == curr_y+i && this->this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->this->board_pieces[i].getX() <<"," << this->this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            }else if(this->this->board_pieces[i].getX()== curr_x &&this-> this->board_pieces[i].getY() == curr_y+i && this->this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->this->board_pieces[i].getX() <<"," <<this-> this->board_pieces[i].getY() << ")";
                something_in_a_way = 1;
                if(this->this->board_pieces[i].getType()=='K' && this->this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            } 
        } else if(new_x >curr_x && new_y == curr_y) {
            if(this->board_pieces[i].getX()== curr_x +i && this->board_pieces[i].getY() == curr_y && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            }else if(this->board_pieces[i].getX()== curr_x +i && this->board_pieces[i].getY() == curr_y && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way =1;
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            }
        } else if(new_x <curr_x && new_y == curr_y) {
            if(this->board_pieces[i].getX()== curr_x -i && this->board_pieces[i].getY() == curr_y && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            }else if(this->board_pieces[i].getX()== curr_x -i && this->board_pieces[i].getY() == curr_y && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 1;
                    if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                        something_in_a_way = 4;
                    }
                    break;
            }
        } else if(new_x = curr_x && new_y < curr_y) {
            if(this->board_pieces[i].getX()== curr_x && this->board_pieces[i].getY() == curr_y-i && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                 break;
            }else if(this->board_pieces[i].getX()== curr_x && this->board_pieces[i].getY() == curr_y-i && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" <<this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                 something_in_a_way = 1;
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            }
            } 
        }
    } else if (piece_type = 'B') {
        for(int i = 1; i <= 8; i++) {
            if(new_x >curr_x && new_y > curr_y) {
                if(this->board_pieces[i].getX()== curr_x +i && this->board_pieces[i].getY() == curr_y+i && this->board_pieces[i].getColour() == piece_colour) {
                    std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";;
                    something_in_a_way = 2;
                    break;
                } else if(this->board_pieces[i].getX()== curr_x +i && this->board_pieces[i].getY() == curr_y+i && this->board_pieces[i].getColour() != piece_colour) {
                    std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 1;
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            }
        } else if(new_x <curr_x && new_y > curr_y) {
            if(this->board_pieces[i].getX()== curr_x -i && this->board_pieces[i].getY() == curr_y+i && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            }else if(this->board_pieces[i].getX()== curr_x -i && this->board_pieces[i].getY() == curr_y+i && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                        }
                break;
            }
        } else if(new_x >curr_x && new_y < curr_y) {
                if(this->board_pieces[i].getX()== curr_x +i && this->board_pieces[i].getY() == curr_y-i && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
        }else if(this->board_pieces[i].getX()== curr_x +i && this->board_pieces[i].getY() == curr_y-i && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 1;
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
        }
    } else if (piece_type = 'k') { 
        if((curr_x +2 ==new_x) && (curr_y +1 ==new_y)) {
            for(int i = 0; i < 32; i++) {
                if(this->board_pieces[i].getX()== curr_x +1 && this->board_pieces[i].getY() == curr_y) {
                    if(piece_colour == this->board_pieces[i].getColour()) {
                        std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 2;
                        break;
                    } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                            something_in_a_way = 4;
                        }
                        break;
                    }
                    
                } else if(this->board_pieces[i].getX()== curr_x +2 && this->board_pieces[i].getY() == curr_y) {
                    if(piece_colour == this->board_pieces[i].getColour()) {
                        std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 2;
                    } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                            something_in_a_way = 4;
                        }
                        break;
                    }
                } else if(this->board_pieces[i].getX()== curr_x +2 && this->board_pieces[i].getY() == curr_y+1) {
                    if(piece_colour == this->board_pieces[i].getColour()) {
                         std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 2;
                        break;
                    } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                            something_in_a_way = 4;
                        }
                        break;
                    }
                }
            } 
        }else if((curr_x +2 ==new_x) && (curr_y -1 ==new_y)) {
            if(this->board_pieces[i].getX()== curr_x +1 && this->board_pieces[i].getY() == curr_y) {
                if(piece_colour == this->board_pieces[i].getColour()) {
                    std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                    break;
                } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                        something_in_a_way = 4;
                        }
                        break;
                    }
                } else if(this->board_pieces[i].getX()== curr_x +2 && this->board_pieces[i].getY() == curr_y) {
                    if(piece_colour == this->board_pieces[i].getColour()) {
                        std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 2;
                    } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                    if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                        something_in_a_way = 4;
                    }
                    break;
                } else if(this->board_pieces[i].getX()== curr_x +2 && this->board_pieces[i].getY() == curr_y-1) {
                    if(piece_colour == this->board_pieces[i].getColour()) {
                        std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 2;
                    } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                            something_in_a_way = 4;
                        }
                        break;
                }
            }
        } else if((curr_x -2 ==new_x) && (curr_y +1 ==new_y)) {
            if(this->board_pieces[i].getX()== curr_x -1 && this->board_pieces[i].getY() == curr_y) {
                if(piece_colour == this->board_pieces[i].getColour()) {
                    std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                            something_in_a_way = 4;
                        }
                        break;
                    }   
                }
            } else if(this->board_pieces[i].getX()== curr_x -2 && this->board_pieces[i].getY() == curr_y) {
                if(piece_colour == this->board_pieces[i].getColour()) {
                    std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                         something_in_a_way = 4;
                        break;
                    }
            } else if(this->board_pieces[i].getX()== curr_x -2 && this->board_pieces[i].getY() == curr_y+1) {
                if(piece_colour == this->board_pieces[i].getColour()) {
                    std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                    break;
                } else if (piece_colour != this->board_pieces[i].getColour()) {
                    std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 1;
                    break;
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                    break;
                }
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            }
        } else if((curr_x -2 ==new_x) && (curr_y -1 ==new_y)) {
             if(this->board_pieces[i].getX()== curr_x -1 && this->board_pieces[i].getY() == curr_y) {
                if(piece_colour == this->board_pieces[i].getColour()) {
                    std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                    break;
                } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                            something_in_a_way = 4;
                        }
                        break;
                }
            } else if(this->board_pieces[i].getX()== curr_x -2 && this->board_pieces[i].getY() == curr_y) {
                if(piece_colour == this->board_pieces[i].getColour()) {
                    std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 2;
                    break;
                } else if (piece_colour != this->board_pieces[i].getColour()) {
                        std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 1;
                        if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                            something_in_a_way = 4;
                        }
                        break;
                 } else if(this->board_pieces[i].getX()== curr_x -2 && this->board_pieces[i].getY() == curr_y-1) {
                    if(piece_colour == this->board_pieces[i].getColour()) {
                        std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                        something_in_a_way = 2;
                        break;
                } else if (piece_colour != this->board_pieces[i].getColour()) {
                    std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 1;
                    if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                        something_in_a_way = 4;
                    }
                    break;
                }
            }
        } 
    } else if(piece_type = 'R') {
        for(int i = 0; i < 32; i++) {}
        if(new_x <curr_x && new_y == curr_y) {
            if(this->board_pieces[i].getX()== curr_x -i && this->board_pieces[i].getY() == curr_y && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            }else if(this->board_pieces[i].getX()== curr_x -i && this->board_pieces[i].getY() == curr_y && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 1;
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
            }
                break;
            } 
        } else if(new_x ==curr_x && new_y > curr_y) {
            if(this->board_pieces[i].getX()== curr_x && this->board_pieces[i].getY() == curr_y+i && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            }else if(this->board_pieces[i].getX()== curr_x && this->board_pieces[i].getY() == curr_y+i && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 1;
                    if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            } 
        } else if(new_x >curr_x && new_y == curr_y) {
            if(this->board_pieces[i].getX()== curr_x +i && this->board_pieces[i].getY() == curr_y && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            }else if(this->board_pieces[i].getX()== curr_x +i && this->board_pieces[i].getY() == curr_y && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 1;
                if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            } 
        } else if(new_x ==curr_x && new_y < curr_y) {
            if(this->board_pieces[i].getX()== curr_x && this->board_pieces[i].getY() == curr_y-i && this->board_pieces[i].getColour() == piece_colour) {
                std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                something_in_a_way = 2;
                break;
            }else if(this->board_pieces[i].getX()== curr_x && this->board_pieces[i].getY() == curr_y-i && this->board_pieces[i].getColour() != piece_colour) {
                std::cout << "An enemy piece in a way. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
                    something_in_a_way = 1;
                    if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                    something_in_a_way = 4;
                }
                break;
            } 
        }
    } else if (piece_type = 'P') {
        for(int i = 0; i <32; i++) {
        if(this->board_pieces[i].getX() == curr_x +1 && this->board_pieces[i].getY() == curr_y +1 && this->board_pieces[i].getColour() !=  piece_colour) {
            std::cout << "An enemy piece that can be eaten. At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
            something_in_a_way = 1;
            if(this->board_pieces[i].getType()=='K' && this->board_pieces[i].getColour() != piece_colour) {
                something_in_a_way = 4;
            }
             break;
        } else if (this->board_pieces[i].getX() == curr_x && this->board_pieces[i].getY() == curr_y +1 && this->board_pieces[i].getColour() ==  piece_colour) {
            std::cout << "Own piece in away At position: (" << this->board_pieces[i].getX() <<"," << this->board_pieces[i].getY() << ")";
            something_in_a_way = 2;
            break;
        }
    }
   

}
        
void serialize_game_to_store( std::string filename) {

std::string serialized = hps::to_string(game);
// std::cout << "Writing " << serialized.size() << " Bytes" << std::endl;
std::cout << "saving the game" <<std::endl;
std::ofstream dataFile;
dataFile.open(filename, std::ios::binary | std::ios::app);
dataFile << serialized <<std::endl;
dataFile.close();
}
void load_game(std::vector<Piece> this->board_pieces, std::string filename) {

std::string line;

std::ifstream dataFile;


dataFile.open(filename, std::ios::binary);
if(dataFile.is_open()) {
    std::string dataRead;
    while(getline(dataFile, line))
    {
// should only be one line to read!
        game = hps::from_string<std::vector<Piece>>(line);
    }
    dataFile.close();
} else {
    std::cout << "File read error!" <<std::endl;
}

}

void set_board_to_start_position() {

Piece KB = Piece(0,4,'K',0,false);
this->board_pieces.push_back(KB);
Piece QB = Piece(0,3,'Q',0, false);
this->board_pieces.push_back(QB);
Piece B1B = Piece(0,2,'B', 0, false);
this->board_pieces.push_back(B1B);
Piece K1B = Piece(0,1,'k', 0, false);
this->board_pieces.push_back(K1B);
Piece R1B = Piece(0,0,'R', 0, false);
this->board_pieces.push_back(R1B);
Piece B2B = Piece(0,5,'B',0, false);
this->board_pieces.push_back(B2B);
Piece K2B = Piece(0,6,'k', 0, false);
this->board_pieces.push_back(K2B);
Piece R2B = Piece(0,7, 'R', 0, false);
this->board_pieces.push_back(R2B);
Piece P1B = Piece(1,0, 'P', 0, false);
this->board_pieces.push_back(P1B);
Piece P2B = Piece(1,1,'P', 0, false);
this->board_pieces.push_back(P2B);
Piece P3B = Piece(1,2,'P', 0, false);
this->board_pieces.push_back(P3B);
Piece P4B = Piece(1,3,'P',0,false);
this->board_pieces.push_back(P4B);
Piece P5B  = Piece(1,4,'P',0,false);
this->board_pieces.push_back(P5B);
Piece P6B = Piece(1,5,'P',0,false);
this->board_pieces.push_back(P6B);
Piece P7B = Piece(1,6,'P',0,false);
this->board_pieces.push_back(P7B);
Piece P8B = Piece(1,7,'P',0,false);
this->board_pieces.push_back(P8B);
Piece KW = Piece(7,4,'K', 1,false);
this->board_pieces.push_back(KW);
Piece QW = Piece(7,3,'Q', 1,false);
this->board_pieces.push_back(QW);
Piece B1W = Piece(7,2,'B',1,false);
this->board_pieces.push_back(B1W);
Piece K1W = Piece(7,1,'k',1,false);
this->board_pieces.push_back(K1W);
Piece R1W = Piece(7,0,'R',1, false);
this->board_pieces.push_back(R1W);
Piece B2W = Piece(7,5,'B',1,false);
this->board_pieces.push_back(B2W);
Piece K2W = Piece(7,6,'k',1,false);
this->board_pieces.push_back(K2W);
Piece R2W = Piece(7,7,'R',1,false);
this->board_pieces.push_back(R2W);
Piece P1W = Piece(6,0,'P',1,false);
this->board_pieces.push_back(P1W);
Piece P2W = Piece(6,1,'P', 1,false);
this->board_pieces.push_back(P2W);
Piece P3W = Piece(6,2,'P', 1, false);
this->board_pieces.push_back(P3W);
Piece P4W = Piece(6,3,'P', 1,false);
this->board_pieces.push_back(P4B);
Piece P5W = Piece(6,4,'P', 1, false);
this->board_pieces.push_back(P5W);
Piece P6W = Piece(6,5,'P', 1, false);
this->board_pieces.push_back(P6W);
Piece P7W = Piece(6,6,'P',1,false);
this->board_pieces.push_back(P7W);
Piece P8W = Piece(6,7,'P',1,false);
this->board_pieces.push_back(P8W);

}
std::string displaySlots {
int number;
std::string filename = "game"
std::cout << "Do you want to play on slot 1,2,3,4,5,6,7,8,9,10?" <<std::endl;
std::cout << "Type number of slot:"<<std::endl;
std::cin >> number;
filename += std::stoi(number);
filename += ".txt";

return filename;
}
};
int main() {
std::string slot = displaySlots();
std::vector<Piece> board_pieces;
Board b = Board(slot, game);

std::vector<Piece> board_pieces;

b.load_game(board_pieces, slot)
int ran_num = rand()%32 +1;
if(board_pieces[ran_num] == null) {
    b.set_board_to_start_position(board_pieces)
}

int turn = 1;
int curr_x;
int curr_y;
int new_x;
int new_y;
char zero = '0';
char type_cur; 
std::string curr_pos;
std::string new_pos:
int is_checkmate

//  std::vector<Piece> board_pieces;
//  set_board_to_atsrt_position(std::vector<Piece> board_pieces);
//  printBoard(board_pieces);

while(is_checkmate!=true) {
int next_state = -1;
    b.printBoard(board_pieces);
    std::cout << "White turm, whhat do you want to move?"<<std::endl; 
    std::cout << "Ccordiantes Pawn A2 to A3. It is given in form A2 A3."<<std::endl; 
    std::cin >>  type_cur;
    std::cin >>  curr_pos
    std::cin >>  new_pos;

    curr_x = curr_pos[0];
    curr_y = curr_pos[1];
    curr_y -=  zero;

    curr_x = new_pos[0];
    curr_y = new_pos[1];
    curr_y -=  zero;
    
next_state = b.check_if_piece_in_a_way(new_x,new_y,curr_x,curr_y, type_cur, 1, board_pieces)
if(next_state == 0) {
    for(int i = 0; i <32; i++) {
        if(b->this->board_pieces[i].getX()== curr_x &&b.this->board_pieces.getY()==curr_y &&b.this->board_pieces.getColour() == 1
        &&b.this->board_pieces.getType() == type_cur) {
           b.this->board_pieces.moveX(new_x, new_y, type_cur)
        }
    }

} else if(next_state == 1) {
    std::cout << "Cant move there"
} else if (next_state == 2) {
    Piece enemy;
    for(int i = 0; i <32; i++) {
        if(b->this->board_pieces[i].getX()== cnew_x &&b.this->board_pieces.getY()==cnew_y &&b.this->board_pieces.getColour() == 0)
            {
            enemy =b.this->board_pieces[i];
            if(enemy.getColour() == 0 && enemy.getType()== 'K') {
                std::cout << "Checkmate!! White wins!!"
                    b.this->board_pieces[i].erase(enemy);
                    checkmate = true;
            } else {
                b->this->board_pieces.erase(enemy);
            }
        }
    }
    for(int i = 0; i <32; i++) {
        if(b.this->board_pieces[i].getX()== curr_x &&b.this->board_pieces.getY()==curr_y &&b.this->board_pieces.getColour() == 1
        &&b.this->board_pieces.getType() == type_cur) {
           b.this->board_pieces[i].moveX(new_x, new_y, type_cur)
        }
    }
} else if(next_state == 4) {
    std::cout << "Check! For black king";
}else if(next_state == -1) {
    std::cout << "Impossible move";
}
 if(checkmate == true) {
     break;
 }
    b.printBoard(board_pieces);
    std::cout << "Black turn, what do you want to move?"<<std::endl; 
    std::cout << "Cordiantes Pawn A2 to A3 is given in for A2 A3"<<std::endl;
    std::cin >> type_cur;
    std::cin >>  curr_x;   
    std::cin >>  curr_y;
std::cin >>  new_x;   
    std::cin >>  new_y;;
next_state = b.check_if_piece_in_a_way(new_x,new_y,curr_x,curr_y, type_cur, 1, board_pieces)
if(next_state == 0) {
    for(int i = 0; i <32; i++) {
        if(b.this->board_pieces[i].getX()== curr_x &&b.this->board_pieces.getY()==curr_y &&b.this->board_pieces.getColour() == 1
        &&b.this->board_pieces.getType() == type_cur) {
           b.this->board_pieces.moveX(new_x, new_y, type_cur)
        }
    }

} else if(next_state == 1) {
    std::cout << "Cant move there, ther is an own piece."<<std::endl;
} else if (next_state == 2) {
    Piece enemy;
    for(int i = 0; i <32; i++) {
        if(b.this->board_pieces[i].getX()== new_x &&b.this->board_pieces.getY()==new_y &&b.this->board_pieces.getColour() == 0)
            {
            enemy =b.this->board_pieces;
            
        }
    }
        for(int i = 0; i <32; i++) {
        if(b.this->board_pieces[i].getX()== curr_x &&b.this->board_pieces.getY()==curr_y &&b.this->board_pieces.getColour() == 1
            &&b.this->board_pieces.getType() == type_cur) {
           b.this->board_pieces.moveX(new_x, new_y, type_cur)
        }
    }

} else if(next_state == -1) {
    std::cout << "Impossible move";
} else if(next_state == 4) {
    std::cout << "Check! For white king";
}

}
return 0;
}
